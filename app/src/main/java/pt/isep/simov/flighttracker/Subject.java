package pt.isep.simov.flighttracker;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ruir on 10/12/2017.
 */

public class Subject implements Parcelable {
    private String uid;
    private String title;
    private String description;

    public Subject(String uid, String title, String description)
    {
        this.uid = uid;
        this.title = title;
        this.description = description;
    }


    protected Subject(Parcel in) {
        uid = in.readString();
        title = in.readString();
        description = in.readString();
    }

    public static final Creator<Subject> CREATOR = new Creator<Subject>() {
        @Override
        public Subject createFromParcel(Parcel in) {
            return new Subject(in);
        }

        @Override
        public Subject[] newArray(int size) {
            return new Subject[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(uid);
        parcel.writeString(title);
        parcel.writeString(description);
    }
}
