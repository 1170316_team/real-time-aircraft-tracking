package pt.isep.simov.flighttracker;

/**
 * Created by ruir on 10/12/2017.
 */

public class AircraftLocation {
    private Integer mAltitude;
    private boolean mAltitudeHoldEnabled;
    private String mAltitudeSource;
    private long mAltitudeTimestamp;
    private boolean mAutoPilotEngaged;
    private Float mBaroSetting;
    private Integer mCategory;

    private int mRawLatitude;
    private int mRawLongitude;
    private Integer mHeading;
    private int mHeadingDelta;

    private String mIcao;
    private String mIdentity;
    private Double mLatitude;
    private Double mLongitude;
    private int mMessageCount;
    private boolean mOnApproach;
    private boolean mOnGround;
    private boolean mReady;
    private long mSeen;
    private long mSeenLatLon;
    private Integer mSelectedAltitude;
    private Integer mSelectedHeading;

    private Integer mSquawk;
    private Integer mStatus;
    private boolean mTcasEnabled;
    private Integer mTrackAngle;
    private boolean mUat;
    private Integer mVelocity;
    private long mVelocityTimestamp;
    private boolean mVerticalNavEnabled;
    private Integer mVerticalRate;

    public Integer getmAltitude() {
        return mAltitude;
    }

    public void setmAltitude(Integer mAltitude) {
        this.mAltitude = mAltitude;
    }

    public boolean ismAltitudeHoldEnabled() {
        return mAltitudeHoldEnabled;
    }

    public void setmAltitudeHoldEnabled(boolean mAltitudeHoldEnabled) {
        this.mAltitudeHoldEnabled = mAltitudeHoldEnabled;
    }

    public String getmAltitudeSource() {
        return mAltitudeSource;
    }

    public void setmAltitudeSource(String mAltitudeSource) {
        this.mAltitudeSource = mAltitudeSource;
    }

    public long getmAltitudeTimestamp() {
        return mAltitudeTimestamp;
    }

    public void setmAltitudeTimestamp(long mAltitudeTimestamp) {
        this.mAltitudeTimestamp = mAltitudeTimestamp;
    }

    public boolean ismAutoPilotEngaged() {
        return mAutoPilotEngaged;
    }

    public void setmAutoPilotEngaged(boolean mAutoPilotEngaged) {
        this.mAutoPilotEngaged = mAutoPilotEngaged;
    }

    public Float getmBaroSetting() {
        return mBaroSetting;
    }

    public void setmBaroSetting(Float mBaroSetting) {
        this.mBaroSetting = mBaroSetting;
    }

    public Integer getmCategory() {
        return mCategory;
    }

    public void setmCategory(Integer mCategory) {
        this.mCategory = mCategory;
    }

    public int getmRawLatitude() {
        return mRawLatitude;
    }

    public void setmRawLatitude(int mRawLatitude) {
        this.mRawLatitude = mRawLatitude;
    }

    public int getmRawLongitude() {
        return mRawLongitude;
    }

    public void setmRawLongitude(int mRawLongitude) {
        this.mRawLongitude = mRawLongitude;
    }

    public Integer getmHeading() {
        return mHeading;
    }

    public void setmHeading(Integer mHeading) {
        this.mHeading = mHeading;
    }

    public int getmHeadingDelta() {
        return mHeadingDelta;
    }

    public void setmHeadingDelta(int mHeadingDelta) {
        this.mHeadingDelta = mHeadingDelta;
    }

    public String getmIcao() {
        return mIcao;
    }

    public void setmIcao(String mIcao) {
        this.mIcao = mIcao;
    }

    public String getmIdentity() {
        return mIdentity;
    }

    public void setmIdentity(String mIdentity) {
        this.mIdentity = mIdentity;
    }

    public Double getmLatitude() {
        return mLatitude;
    }

    public void setmLatitude(Double mLatitude) {
        this.mLatitude = mLatitude;
    }

    public Double getmLongitude() {
        return mLongitude;
    }

    public void setmLongitude(Double mLongitude) {
        this.mLongitude = mLongitude;
    }

    public int getmMessageCount() {
        return mMessageCount;
    }

    public void setmMessageCount(int mMessageCount) {
        this.mMessageCount = mMessageCount;
    }

    public boolean ismOnApproach() {
        return mOnApproach;
    }

    public void setmOnApproach(boolean mOnApproach) {
        this.mOnApproach = mOnApproach;
    }

    public boolean ismOnGround() {
        return mOnGround;
    }

    public void setmOnGround(boolean mOnGround) {
        this.mOnGround = mOnGround;
    }

    public boolean ismReady() {
        return mReady;
    }

    public void setmReady(boolean mReady) {
        this.mReady = mReady;
    }

    public long getmSeen() {
        return mSeen;
    }

    public void setmSeen(long mSeen) {
        this.mSeen = mSeen;
    }

    public long getmSeenLatLon() {
        return mSeenLatLon;
    }

    public void setmSeenLatLon(long mSeenLatLon) {
        this.mSeenLatLon = mSeenLatLon;
    }

    public Integer getmSelectedAltitude() {
        return mSelectedAltitude;
    }

    public void setmSelectedAltitude(Integer mSelectedAltitude) {
        this.mSelectedAltitude = mSelectedAltitude;
    }

    public Integer getmSelectedHeading() {
        return mSelectedHeading;
    }

    public void setmSelectedHeading(Integer mSelectedHeading) {
        this.mSelectedHeading = mSelectedHeading;
    }

    public Integer getmSquawk() {
        return mSquawk;
    }

    public void setmSquawk(Integer mSquawk) {
        this.mSquawk = mSquawk;
    }

    public Integer getmStatus() {
        return mStatus;
    }

    public void setmStatus(Integer mStatus) {
        this.mStatus = mStatus;
    }

    public boolean ismTcasEnabled() {
        return mTcasEnabled;
    }

    public void setmTcasEnabled(boolean mTcasEnabled) {
        this.mTcasEnabled = mTcasEnabled;
    }

    public Integer getmTrackAngle() {
        return mTrackAngle;
    }

    public void setmTrackAngle(Integer mTrackAngle) {
        this.mTrackAngle = mTrackAngle;
    }

    public boolean ismUat() {
        return mUat;
    }

    public void setmUat(boolean mUat) {
        this.mUat = mUat;
    }

    public Integer getmVelocity() {
        return mVelocity;
    }

    public void setmVelocity(Integer mVelocity) {
        this.mVelocity = mVelocity;
    }

    public long getmVelocityTimestamp() {
        return mVelocityTimestamp;
    }

    public void setmVelocityTimestamp(long mVelocityTimestamp) {
        this.mVelocityTimestamp = mVelocityTimestamp;
    }

    public boolean ismVerticalNavEnabled() {
        return mVerticalNavEnabled;
    }

    public void setmVerticalNavEnabled(boolean mVerticalNavEnabled) {
        this.mVerticalNavEnabled = mVerticalNavEnabled;
    }

    public Integer getmVerticalRate() {
        return mVerticalRate;
    }

    public void setmVerticalRate(Integer mVerticalRate) {
        this.mVerticalRate = mVerticalRate;
    }
}
