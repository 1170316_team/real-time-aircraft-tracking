package com.flightaware.android.flightfeeder.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.NavigationView.OnNavigationItemSelectedListener;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.flightaware.android.flightfeeder.App;
import com.flightaware.android.flightfeeder.R;
import com.flightaware.android.flightfeeder.adapters.PlaneAdapter;
import com.flightaware.android.flightfeeder.analyzers.Aircraft;
import com.flightaware.android.flightfeeder.analyzers.Analyzer;
import com.flightaware.android.flightfeeder.analyzers.RecentAircraftCache;
import com.flightaware.android.flightfeeder.services.ControllerService;
import com.flightaware.android.flightfeeder.services.LocationService;
import com.flightaware.android.flightfeeder.util.MovingAverage;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import pt.isep.simov.flighttracker.AircraftLocation;
import pt.isep.simov.flighttracker.Subject;

public class MainActivity extends AppCompatActivity implements
        OnNavigationItemSelectedListener, OnItemClickListener,
        ServiceConnection {

    public static final String ACTION_LOGIN = "com.flightaware.android.flightfeeder.LOGIN";
    public static final String ACTION_MODE_CHANGE = "com.flightaware.android.flightfeeder.MODE_CHANGE";
    public static final String ACTION_UPDATE_1HERTZ = "com.flightaware.android.flightfeeder.UPDATE_1HERTZ";
    public static final String ACTION_UPDATE_RX = "com.flightaware.android.flightfeeder.UPDATE_RX";
    public static final String ACTION_UPDATE_TX = "com.flightaware.android.flightfeeder.UPDATE_TX";
    private static final String ACTION_USB_PERMISSION = "com.flightaware.android.flightfeeder.USB_PERMISSION";

    private static final int LOCATION_REQUEST_CODE = 100;

    private Thread m1HertzUpdater;
    private AlertDialog mAlert;
    private long mBackPressedTime;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private IntentFilter mFilter;
    private NavigationView mNavView;
    private int mOffDelay = 50;
    private BroadcastReceiver mPermissionReceiver;
    private PlaneAdapter mPlaneAdapter;
    private ListView mPlaneList;
    private ArrayList<Aircraft> mPlanes = new ArrayList<Aircraft>();
    private Toast mPressBackToast;
    private TextView mRange;
    private TextView mRate;
    private BroadcastReceiver mReceiver;
    private ImageView mRx;
    private ImageView mTx;
    private ControllerService mService;
    private TextView mUsernameView;
    private float radarDistance = 0.0f;
    DatabaseReference database;
    ArrayList itemsArrayList = new ArrayList();

    // LocationManager mLocationManager;
    // LocationRequest mLocationRequest = new LocationRequest();

    //Location deviceLocation;

    private FusedLocationProviderClient mFusedLocationClient;
    protected Location mLastLocation;

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerVisible(mNavView)) {
            mDrawerLayout.closeDrawers();
            return;
        }

        long currentTime = System.currentTimeMillis();
        if (currentTime - mBackPressedTime > 2000 /* Toast.LENGTH_LONG  */) {
            mPressBackToast.show();
            mBackPressedTime = currentTime;
        } else {
            mPressBackToast.cancel();
            super.onBackPressed();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        mDrawerToggle.onConfigurationChanged(newConfig);
    }


  /*  private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            //your code here
            deviceLocation = location;

            Toast.makeText(getBaseContext(), "Nova localização: " + location.getLatitude(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };*/

    private void getLastLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            mLastLocation = task.getResult();


                        } else {
                            Log.e("Location", "Device location not found");
                        }
                    }
                });
    }

    @SuppressLint("ShowToast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Locale locale = new Locale(App.sPrefs.getString("pref_language", "pt"));
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        radarDistance = Float.valueOf(App.sPrefs.getString("radar_detection_radios", "100"));

        database = FirebaseDatabase.getInstance().getReference();

       /* AircraftLocation aircraftLocation = new AircraftLocation();
        aircraftLocation.setmAltitude(112);
        aircraftLocation.setmAutoPilotEngaged(true);
        aircraftLocation.setmIcao("WJA4");
        database.child("aircrafts").child("WJA4").setValue(aircraftLocation);*/
        // database.setValue(aircraftLocation);

        //myRef.setValue("Ola, Mundo!");


        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        /*mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        int LOCATION_REFRESH_TIME = 600; //1 minuto
        int LOCATION_REFRESH_DISTANCE = 100; // 100 metros

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_REFRESH_TIME,
                LOCATION_REFRESH_DISTANCE, mLocationListener);*/

        startService(new Intent(this, ControllerService.class));

        if (!App.sPrefs.getBoolean("override_location", false))
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager
                    .PERMISSION_GRANTED) {

                startService(new Intent(this, LocationService.class));
            } else {
                ActivityCompat
                        .requestPermissions(
                                this,
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                LOCATION_REQUEST_CODE);
            }
        else if (LocationService.sLocation == null
                && App.sPrefs.contains("latitude")
                && App.sPrefs.contains("longitude")) {
            float lat = App.sPrefs.getFloat("latitude", 0);
            float lon = App.sPrefs.getFloat("longitude", 0);

            LocationService.sLocation = new LatLng(lat, lon);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mNavView = (NavigationView) findViewById(R.id.navigation_view);
        mNavView.setNavigationItemSelectedListener(this);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar,
                R.string.app_name, R.string.app_name);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        mRx = (ImageView) findViewById(R.id.rx);
        mTx = (ImageView) findViewById(R.id.tx);
        mRate = (TextView) findViewById(R.id.rate);
        mRange = (TextView) findViewById(R.id.range);
        mPlaneList = (ListView) findViewById(R.id.output);
        mPressBackToast = Toast.makeText(this, R.string.text_press_again,
                Toast.LENGTH_LONG);

        mPlaneAdapter = new PlaneAdapter(this, mPlanes);
        mPlaneList.setAdapter(mPlaneAdapter);
        mPlaneList.setOnItemClickListener(this);

        final Runnable rxOff = new Runnable() {
            @Override
            public void run() {
                mRx.setImageResource(R.drawable.data_off);
            }
        };

        final Runnable txOff = new Runnable() {
            @Override
            public void run() {
                mTx.setImageResource(R.drawable.data_off);
            }
        };

        mFilter = new IntentFilter();
        mFilter.addAction(ACTION_UPDATE_RX);
        mFilter.addAction(ACTION_UPDATE_TX);
        mFilter.addAction(ACTION_UPDATE_1HERTZ);
        mFilter.addAction(ACTION_MODE_CHANGE);
        mFilter.addAction(ACTION_LOGIN);

        mReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (!TextUtils.isEmpty(action)) {
                    if (action.equals(ACTION_UPDATE_RX)) {
                        mRx.removeCallbacks(rxOff);
                        mRx.setImageResource(R.drawable.data_on);
                        mRx.postDelayed(rxOff, mOffDelay);
                    } else if (action.equals(ACTION_UPDATE_TX)) {
                        mTx.removeCallbacks(txOff);
                        mTx.setImageResource(R.drawable.data_on);
                        mTx.postDelayed(txOff, 200);
                    } else if (action.equals(ACTION_UPDATE_1HERTZ)) {
                        mRange.setText(String.format("%.1f", Analyzer.sRange));

                        double rate = MovingAverage.getCurrentAverage();

                        int delay = (int) (1000 / rate);
                        mOffDelay = Math.min(delay, 200);

                        mRate.setText(String.format("%.1f", rate));

                        int number_plains = mPlanes.size();
                        mPlanes.clear();
                        ArrayList<Aircraft> currentAircrafts = RecentAircraftCache
                                .getActiveAircraftList(true);
                        mPlanes.addAll(currentAircrafts);


                        getLastLocation();

                        if (mLastLocation != null)
                        {
                            float distance = 99999999;
                            // check closest aircraft
                            for (Aircraft a:mPlanes) {


                                if (a.getLatitude() != null && a.getLongitude() != null)
                                {
                                    Location locationA = new Location("point A");
                                    locationA.setLatitude(a.getLatitude());
                                    locationA.setLongitude(a.getLatitude());

                                    float newDistance = mLastLocation.distanceTo(locationA) / 1000.0f; //in km

                                    if (newDistance < distance )
                                        distance = newDistance;
                                }


                            }

                            Log.i("last location", mLastLocation.getLatitude() + " Lat " + mLastLocation.getLongitude() + " Lon");
                            Log.i("distance", String.valueOf(distance) + " Km");

                            if (distance < radarDistance)
                            {

                                NotificationCompat.Builder mBuilder =
                                        new NotificationCompat.Builder(getBaseContext())
                                                .setSmallIcon(R.drawable.fa_logo_white)
                                                .setContentTitle(getString(R.string.notificationTitle))
                                                .setContentText(getString(R.string.closestAircraft) + " " + distance + " " + getString(R.string.metersofdistance));

                                // Sets an ID for the notification
                                int mNotificationId = 001;
                                // Gets an instance of the NotificationManager service
                                NotificationManager mNotifyMgr =
                                        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

                                // Builds the notification and issues it.
                                mNotifyMgr.notify(mNotificationId, mBuilder.build());

                                if(mp.isPlaying())
                                {
                                    mp.stop();
                                }

                                try {
                                    mp.reset();
                                    AssetFileDescriptor afd;
                                    afd = getAssets().openFd("radar.mp3");
                                    mp.setDataSource(afd.getFileDescriptor(),afd.getStartOffset(),afd.getLength());
                                    mp.prepare();
                                    mp.start();
                                } catch (IllegalStateException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }


                        //TODO: não é muito bom para a bateria!!!
                        // mudar o sample rate!
                        updateFireBase(currentAircrafts);

                        mPlaneAdapter.notifyDataSetChanged();
                    } else if (action.equals(ACTION_MODE_CHANGE)) {
                        mRate.setText("0.0");
                        mRange.setText("0.0");

                        if (mService != null)
                            setTitle(mService.isUat());
                    }
                }
            }
        };

        mPermissionReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                synchronized (this) {
                    UsbDevice device = (UsbDevice) intent
                            .getParcelableExtra(UsbManager.EXTRA_DEVICE);

                    if (intent.getBooleanExtra(
                            UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if (device != null) {
                            Editor editor = App.sPrefs.edit();
                            editor.putBoolean("usb_permission_granted", true);
                            editor.commit();

                            startListening(device);
                        } else {
                            Editor editor = App.sPrefs.edit();
                            editor.putBoolean("usb_permission_granted", false);
                            editor.commit();
                        }
                    } else {
                        Editor editor = App.sPrefs.edit();
                        editor.putBoolean("usb_permission_granted", false);
                        editor.commit();

                        finish();
                    }
                }
            }

        };

        registerReceiver(mPermissionReceiver, new IntentFilter(
                ACTION_USB_PERMISSION));

        bindService(new Intent(this, ControllerService.class), this,
                Context.BIND_IMPORTANT);
    }

    public void updateFireBase(ArrayList<Aircraft> currentAircrafts)
    {
        AircraftLocation aLocation;
        for (Aircraft a:currentAircrafts
             ) {
            aLocation = new AircraftLocation();


            try {



                aLocation.setmIcao(a.getIcao());

                aLocation.setmAltitude(a.getAltitude());
                aLocation.setmAltitudeHoldEnabled(a.isAltitudeHoldEnabled());
                aLocation.setmAltitudeSource(a.getAltitudeSource());
                aLocation.setmAltitudeTimestamp(a.getAltitude());
                aLocation.setmAutoPilotEngaged(a.isAutoPilotEngaged());
                aLocation.setmBaroSetting(a.getBaroSetting());
                aLocation.setmCategory(a.getCategory());

                aLocation.setmRawLatitude(a.getEvenPosition().getRawLatitude());
                aLocation.setmRawLongitude(a.getEvenPosition().getRawLongitude());
                aLocation.setmHeading(a.getHeading());
                aLocation.setmHeadingDelta(a.getHeadingDelta());

                aLocation.setmIcao(a.getIcao());
                aLocation.setmIdentity(a.getIdentity());
                aLocation.setmLatitude(a.getLatitude());
                aLocation.setmLongitude(a.getLongitude());
                aLocation.setmMessageCount(a.getMessageCount());
                aLocation.setmOnApproach(a.isOnApproach());
                aLocation.setmOnGround(a.isOnGround());
              //  aLocation.setmReady(a.isReady());
                aLocation.setmSeen(a.getSeen());
                aLocation.setmSeenLatLon(a.getSeenLatLon());
                aLocation.setmSelectedAltitude(a.getSelectedAltitude());
                aLocation.setmSelectedHeading(a.getSelectedHeading());

                aLocation.setmSquawk(a.getSquawk());
                aLocation.setmStatus(a.getStatus());
                aLocation.setmTcasEnabled(a.isTcasEnabled());
                aLocation.setmTrackAngle(a.getTrackAngle());
                aLocation.setmUat(a.isUat());
                aLocation.setmVelocity(a.getVelocity());
                aLocation.setmVelocityTimestamp(a.getVelocity());
                aLocation.setmVerticalNavEnabled(a.isVerticalNavEnabled());
                aLocation.setmVerticalRate(a.getVerticalRate());


                if (aLocation.getmIcao() != null)
                {
                    database.child("aircrafts").child(aLocation.getmIcao()).setValue(aLocation);
                }

            }catch (Exception e)
            {

            }
        }
    }



    @Override
    public void onDestroy() {
        if (mAlert != null)
            mAlert.dismiss();

        if (!App.sPrefs.getBoolean("pref_background", true) || mService == null
                || !mService.isScanning())
            stopService(new Intent(this, ControllerService.class));
        else
            mService.showNotification();

        unregisterReceiver(mPermissionReceiver);

        unbindService(this);

        super.onDestroy();
    }

    @Override
    public void onItemClick(AdapterView<?> listView, View view, int position,
                            long id) {
        Aircraft aircraft = (Aircraft) listView.getItemAtPosition(position);

        if (aircraft == null)
            return;

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("https://flightaware.com/live/flight/"
                + aircraft.getIdentity()));

        startActivity(intent);
    }

    final MediaPlayer mp = new MediaPlayer();

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        mDrawerLayout.closeDrawers();

        int id = menuItem.getItemId();

        if (id == R.id.drawer_map)
            startActivity(new Intent(this, MapActivity.class));

        else if (id == R.id.drawer_settings)
            startActivity(new Intent(this, SettingsActivity.class));

        return true;
    }

    @Override
    public void onNewIntent(Intent intent) {
        if (intent != null) {
            String action = intent.getAction();

            if (TextUtils.isEmpty(action))
                return;

            UsbDevice device = null;

            if (action
                    .equals("android.hardware.usb.action.USB_DEVICE_ATTACHED")) {
                synchronized (this) {
                    device = (UsbDevice) intent
                            .getParcelableExtra(UsbManager.EXTRA_DEVICE);

                    if (device != null)
                        startListening(device);
                    else if (mAlert == null || !mAlert.isShowing())
                        showNoDongle();
                }
            } else if (action
                    .equals("android.hardware.usb.action.USB_DEVICE_DETACHED")) {
                if (mAlert == null || !mAlert.isShowing())
                    showNoDongle();

                if (mService != null)
                    mService.stopScanning(false);
            } else {
//                device = UsbDvbDetector.isValidDeviceConnected(this);

                if (device != null) {
                    UsbManager usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
                    if (usbManager.hasPermission(device)
                            || App.sPrefs.getBoolean("usb_permission_granted",
                            false))
                        startListening(device);
                    else {
                        PendingIntent permission = PendingIntent.getBroadcast(
                                this, 0, new Intent(ACTION_USB_PERMISSION), 0);

                        usbManager.requestPermission(device, permission);
                    }
                } else if (mAlert == null || !mAlert.isShowing())
                    showNoDongle();
            }
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        mDrawerToggle.syncState();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if (requestCode == LOCATION_REQUEST_CODE && grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startService(new Intent(this, LocationService.class));
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        database.child("aircrafts").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                itemsArrayList.clear();
                for (DataSnapshot subjectDataSnapshot : dataSnapshot.getChildren()) {
                    AircraftLocation aircraftLocation = subjectDataSnapshot.getValue(AircraftLocation.class);
                    itemsArrayList.add(aircraftLocation);
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });


        String scanMode = App.sPrefs.getString("pref_scan_mode", "ADSB");

        if (scanMode.equals("ADSB"))
            setTitle(false);
        else if (scanMode.equals("UAT"))
            setTitle(true);
        else if (mService != null)
            setTitle(mService.isUat());

        mPlaneList.setKeepScreenOn(App.sPrefs.getBoolean("pref_keep_screen_on",
                false));

        if (mService != null)
            mService.stopForeground(true);

        App.sBroadcastManager.registerReceiver(mReceiver, mFilter);

        if (m1HertzUpdater == null) {
            m1HertzUpdater = new Thread() {
                @Override
                public void run() {
                    while (true) {
                        App.sBroadcastManager.sendBroadcast(new Intent(
                                ACTION_UPDATE_1HERTZ));

                        SystemClock.sleep(1000);
                    }
                }
            };
            m1HertzUpdater.start();
        }
    }

    @Override
    public void onStop() {
        if (m1HertzUpdater != null) {
            m1HertzUpdater.interrupt();
            m1HertzUpdater = null;
        }

        App.sBroadcastManager.unregisterReceiver(mReceiver);

        super.onStop();
    }

    private void setTitle(boolean uatMode) {
        String title = getString(R.string.app_name);

        if (uatMode)
            title += " - " + getString(R.string.text_978_mhz);
        else
            title += " - " + getString(R.string.text_1090_mhz);

        getSupportActionBar().setTitle(title);
    }

    private void showNoDongle() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,
                R.style.Theme_AppCompat_Light_Dialog_Alert);
        builder.setTitle(R.string.dialog_no_usb_device_title);
        builder.setMessage(R.string.dialog_no_usb_device_msg);
        builder.setPositiveButton(android.R.string.ok, null);

        mAlert = builder.create();
        mAlert.show();
    }

    private void startListening(UsbDevice device) {
        System.out.println("1");
        if (mService != null) {
            mService.setUsbDevice(device);

            mService.startScanning();
        }

        if (mAlert != null && mAlert.isShowing())
            mAlert.dismiss();
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        mService = ((ControllerService.LocalBinder) service).getService();
        mService.stopForeground(true);

        onNewIntent(getIntent());
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
    }
}
